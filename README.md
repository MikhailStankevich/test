<h1>Тестовый стенд</h1>
<p>
Yii2 basic / php7.4
</p>

***

<h3>Разворачиваем проект:</h3>
<ol>
    <li><code>git clone git@gitlab.com:MikhailStankevich/test.git</code></li>
    <li><code>cd test</code></li>
    <li><code>docker compose up -d</code></li>
    <li>добавляем запись в /etc/hosts<br>127.0.0.1 front.test</li>
    <li>настраиваем подключение к БД</li>
    <li>применяем миграции <code>docker exec test-php-fpm php /app/yii migrate --interactive=0</code></li>
</ol>

***

<h3>Создание воркера</h3>
<code>docker exec test-php-fpm php /app/yii sv/create <worker_name> <worker_path> <num_proc></code>

Пример<br>
<code>docker exec test-php-fpm php /app/yii sv/create testworker /app/workers/TestWorker.php 2</code>

<h3>Обновление воркера</h3>
<code>docker exec test-php-fpm php /app/yii sv/update <worker_name> <num_proc></code>

Пример<br>
<code>docker exec test-php-fpm php /app/yii sv/update testworker 2</code>

<h3>Посмотреть информацию о запущенных воркерах</h3>
<code>docker exec test-php-fpm php /app/yii sv/info</code>