<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mysql:3306;dbname=yii2basic',
    'username' => 'admin',
    'password' => 'secret',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
