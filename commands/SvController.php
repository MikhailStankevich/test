<?php

namespace app\commands;

use app\models\worker\Worker;
use Supervisor\Api;
use Supervisor\Configuration\Configuration;
use Supervisor\Configuration\Section\Program;
use Supervisor\Configuration\Section\Supervisord;
use Supervisor\Configuration\Writer\IniFileWriter;
use yii\console\Controller;
use yii\db\StaleObjectException;

/**
 * Class SvController
 */
class SvController extends Controller
{
    public function actionWatcher()
    {
        while (true) {
            $workers = Worker::findAll(['flag' => true]);

            if ($workers) {
                foreach ($workers as $worker) {
                    $config = new Configuration();

                    $section = new Supervisord(['identifier' => 'supervisor']);
                    $config->addSection($section);

                    $section = new Program($worker->name, [
                        'process_name' => '%(program_name)s_%(process_num)02d',
                        'command' => $worker->command,
                        'autostart' => (bool)$worker->autostart,
                        'autorestart' => (bool)$worker->autorestart,
                        'user' => $worker->user,
                        'numprocs' => (int)$worker->numprocs,
                        'stdout_logfile' => $worker->outlog,
                        'stderr_logfile' => $worker->errlog,
                    ]);
                    $config->addSection($section);

                    $writer = new IniFileWriter('/app/docker/php-fpm/supervisor/conf.d/' . $worker->name . '.conf');
                    $writer->write($config);

                    $worker->flag = 0;
                    $worker->update();
                }

                echo shell_exec('supervisorctl reread');
                echo shell_exec('supervisorctl update');
            }
        }
    }

    /**
     * @param $name
     * @param $command
     * @param $numprocs
     */
    public function actionCreate($name, $command, $numprocs)
    {
        $worker = new Worker();

        $worker->name = $name;
        $worker->command = 'php ' . $command;
        $worker->autostart = 1;
        $worker->autorestart = 1;
        $worker->user = 'www-data';
        $worker->numprocs = $numprocs;
        $worker->outlog = '/var/log/supervisor/' . $name . '.log';
        $worker->errlog = '/var/log/supervisor/' . $name . '_error.log';
        $worker->flag = 1;

        if ($worker->save()) {
            echo "Worker created \n";
        } else {
            print_r($worker->getErrors());
        }
    }

    public function actionUpdate($name, $numprocs)
    {
        $worker = Worker::findOne(['name' => $name]);
        $worker->numprocs = $numprocs;
        $worker->flag = 1;
        $worker->update();
    }

    public function actionInfo()
    {
        $api = new Api('127.0.0.1', 9001 /* username, password */);
        print_r($api->getAllProcessInfo());
    }

    public function actionRestart()
    {
        $api = new Api('127.0.0.1', 9001 /* username, password */);
        print_r($api->restart());
    }
}