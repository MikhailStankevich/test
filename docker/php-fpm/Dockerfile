FROM php:7.4.23-fpm

#Update apt
RUN apt-get update

#Install necessary program
RUN apt-get -y install \
    screen \
    git \
    vim \
    zip \
    nodejs \
    npm

#Install preprocessors by npm
RUN npm install -g sass typescript

#Install necessary libs
RUN apt-get -y install \
    zlib1g-dev \
    libwebp-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libxpm-dev \
    libfreetype6-dev \
    libicu-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    libonig-dev \
    librabbitmq-dev \
    libssh-dev \
    libsodium-dev \
    zlib1g-dev \
    libzip-dev \
    g++ \
    procps

#Install lib for PostgreSQL
RUN apt-get install -y libpq-dev

#Configure GD
RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
  && docker-php-ext-install -j$(nproc) gd xmlrpc \
  && docker-php-source delete

#Install php extension for PostgreSQL
RUN docker-php-ext-install pdo pdo_pgsql pdo_mysql

#Install and configure other php extensions
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl \
    bcmath \
    xml \
    curl \
    mbstring \
    json \
    pcntl \
    sodium \
    zip \
    gd \
    soap

RUN pecl install amqp \
    && pecl install dbase \
    && docker-php-ext-enable amqp \
    zip \
    sodium \
    dbase \
    gd

RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer

# Install Supervisor.
RUN \
  apt-get update && \
  apt-get install -y supervisor && \
  rm -rf /var/lib/apt/lists/* && \
  sed -i 's/^\(\[supervisord\]\)$/\1\nnodaemon=true/' /etc/supervisor/supervisord.conf

# Define default command.
CMD ["supervisord", "-c", "/etc/supervisor/supervisord.conf"]