<?php

namespace app\models\worker;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[WorkerRecord]].
 *
 * @see WorkerRecord
 */
class WorkerQuery extends ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return WorkerRecord[]|array
     */
    public function all($db = null): array
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return WorkerRecord|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
