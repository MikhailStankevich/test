<?php

namespace app\models\worker;

use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "worker".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $command
 * @property int|null $autostart
 * @property int|null $autorestart
 * @property string|null $user
 * @property int|null $numprocs
 * @property string|null $outlog
 * @property string|null $errlog
 * @property int|null $flag
 * @property string $created
 * @property string|null $updated
 */
class WorkerRecord extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'worker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['autostart', 'autorestart', 'numprocs', 'flag'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name', 'command', 'user', 'outlog', 'errlog'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'command' => 'Command',
            'autostart' => 'Autostart',
            'autorestart' => 'Autorestart',
            'user' => 'User',
            'numprocs' => 'Numprocs',
            'outlog' => 'Outlog',
            'errlog' => 'Errlog',
            'flag' => 'Flag',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    BaseActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return WorkerQuery the active query used by this AR class.
     */
    public static function find(): WorkerQuery
    {
        return new WorkerQuery(get_called_class());
    }
}
