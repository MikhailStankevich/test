<?php

use yii\db\Migration;

/**
 * Class m210913_085759_add_worker_table
 */
class m210913_085759_add_worker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'command' => $this->string(255),
            'autostart' => $this->boolean()->defaultValue(true),
            'autorestart' => $this->boolean()->defaultValue(true),
            'user' => $this->string(255)->defaultValue('www-data'),
            'numprocs' => $this->integer(11)->defaultValue(1),
            'outlog' => $this->string(255),
            'errlog' => $this->string(255),
            'flag' => $this->boolean()->defaultValue(true),
            'created' => $this->integer(),
            'updated' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('worker');
    }
}
